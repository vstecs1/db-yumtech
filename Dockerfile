# base image 
FROM base

# set working directory
RUN mkdir /app
WORKDIR /app


#ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
#COPY public /usr/src/app/public
#COPY src /usr/src/app/src

#COPY package.json /app/package.json

#RUN npm install
COPY . .
#RUN npm install react-scripts -g
RUN chmod 500 /app/sto.sh
RUN npm install . 
RUN rm -rf /app/public/uploads
RUN ln -s /mnt/jojo /app/public/uploads
#RUN npm run build

# start app
#EXPOSE 80
ENTRYPOINT ["/app/sto.sh"]
